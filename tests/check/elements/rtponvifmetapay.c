/*
 * rtponvifmetapay.c
 *
 * Copyright (C) Alexander Muravev <aleksandrm8@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#include <gst/check/gstcheck.h>
#include <gst/rtp/gstrtpbuffer.h>
#include <gst/check/gstharness.h>

static gboolean
compare_bufs (GstBuffer * raw_buf, GstBuffer * payloaded_buf)
{
  GstMapInfo raw_buf_map, payloaded_buf_map;
  gboolean result;
  glong header_size = 12;

  gst_buffer_map (raw_buf, &raw_buf_map, GST_MAP_READ);
  gst_buffer_map (payloaded_buf, &payloaded_buf_map, GST_MAP_READ);
  if (payloaded_buf_map.size < header_size) {
    return FALSE;
  }

  if (raw_buf_map.size != payloaded_buf_map.size - header_size) {
    return FALSE;
  }

  result = memcmp (raw_buf_map.data,
      payloaded_buf_map.data + header_size, raw_buf_map.size) == 0;

  gst_buffer_unmap (raw_buf, &raw_buf_map);
  gst_buffer_unmap (payloaded_buf, &payloaded_buf_map);

  return result;
}

GST_START_TEST (test_pay)
{
  GstBuffer *in, *out;
  GstMapInfo map;
  GstCaps *caps;
  GstHarness *h;

  const char *data = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\
                <tt:MetaDataStream xmlns:tt=" "\"http://www.onvif.org/ver10/schema\">\n\
                 <tt:VideoAnalytics>\n\
                 <tt:Frame UtcTime=\"2008-10-10T12:24:57.321\">\n\
                 </tt:Frame>\n\
                 <tt:Frame UtcTime=\"2008-10-10T12:24:57.621\">\n\
                 </tt:Frame>\n\
                 </tt:VideoAnalytics>\n\
                </tt:MetaDataStream>\n\
                <?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\
                <tt:MetaDataStream xmlns:tt=" "\"http://www.onvif.org/ver10/schema\">\n\
                 <tt:Event>\n\
                 <wsnt:NotficationMessage>\n\
                 <wsnt:Message>\n\
                 <tt:Message UtcTime= \"2008-10-10T12:24:57.628\">\n\
                 </tt:Message>\n\
                 </wsnt:Message>\n\
                 </wsnt:NotficationMessage>\n\
                 </tt:Event>\n\
                </tt:MetaDataStream> ";

  h = gst_harness_new_parse ("rtponvifmetapay");
  caps = gst_caps_new_simple ("application/onvif-metadata",
      "parsed", G_TYPE_BOOLEAN, "true", NULL);
  gst_harness_set_src_caps (h, caps);

  in = gst_harness_create_buffer (h, strlen (data) + 1);
  gst_buffer_map (in, &map, GST_MAP_READWRITE);
  memcpy (map.data, data, strlen (data) + 1);
  gst_buffer_unmap (in, &map);
  gst_buffer_ref (in);
  GST_BUFFER_FLAG_SET (in, GST_BUFFER_FLAG_MARKER);
  out = gst_harness_push_and_pull (h, in);

  fail_unless (compare_bufs (in, out) == TRUE);

  gst_buffer_unref (in);
  gst_buffer_unref (out);
  gst_harness_teardown (h);
}

GST_END_TEST;

static Suite *
onviftimestamp_suite (void)
{
  Suite *s = suite_create ("onvifmetapay");
  TCase *tc_chain;

  tc_chain = tcase_create ("pay");
  suite_add_tcase (s, tc_chain);
  tcase_add_test (tc_chain, test_pay);

  return s;
}

GST_CHECK_MAIN (onviftimestamp);
